//
//  validations.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 22/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//



extension Collection where Element == Int {
    var digitoCPF: Int {
        var number = count + 2
        let digit = 11 - reduce(into: 0) {
            number -= 1
            $0 += $1 * number
        } % 11
        return digit > 9 ? 0 : digit
    }
}
extension String {

    var isValidCPF: Bool {
           let numbers = compactMap(\.wholeNumberValue)
           guard numbers.count == 11 && Set(numbers).count != 1 else { return false }
           return numbers.prefix(9).digitoCPF == numbers[9] &&
                  numbers.prefix(10).digitoCPF == numbers[10]
       }
}
