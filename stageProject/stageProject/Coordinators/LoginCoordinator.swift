//
//  LoginCoodinator.swift
//  stageProject
//
//  Created by Everson Pereira Trindade on 03/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

protocol BackToWelcomeViewControllerDelegate: class {
    func navigateBackToWelcomePage(newOrderCoordinator: LoginCoordinator)
}

class LoginCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    weak var delegate: BackToWelcomeViewControllerDelegate?

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let loginCPFViewController : LoginCPFViewController = LoginCPFViewController()
        loginCPFViewController.delegate = self
        self.navigationController.pushViewController(loginCPFViewController, animated: true)
    }
}

extension LoginCoordinator : LoginCPFViewControllerDelegate {
    
    func navigateToNextPage(){
         let loginPasswordCoordinator = LoginPasswordCoordinator(navigationController: navigationController)
              loginPasswordCoordinator.delegate = self
              childCoordinators.append(loginPasswordCoordinator)
              loginPasswordCoordinator.start()
    
    }
      // Navigate to Welcome page
      func navigateBackToWelcomePage() {
          self.delegate?.navigateBackToWelcomePage(newOrderCoordinator: self)
      }
    
}

extension LoginCoordinator: BackToLoginCPFViewControllerDelegate{
    func navigateBackToLoginCPFPage(newOrderCoordinator: LoginPasswordCoordinator) {
        navigationController.popViewController(animated: true)
        childCoordinators.removeLast()
    }
}
