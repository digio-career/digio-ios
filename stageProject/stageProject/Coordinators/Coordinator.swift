//
//  Coordinator.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 02/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    
    var childCoordinators: [Coordinator] { get set }
    
    // All coordinators will be initilised with a navigation controller
    init(navigationController: UINavigationController)

    func start()
}
