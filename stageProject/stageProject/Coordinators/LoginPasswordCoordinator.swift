//
//  LoginPasswordCoordinator.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 19/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

protocol BackToLoginCPFViewControllerDelegate: class {
    func navigateBackToLoginCPFPage(newOrderCoordinator: LoginPasswordCoordinator)
}

class LoginPasswordCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    weak var delegate: BackToLoginCPFViewControllerDelegate?

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let loginPasswordViewController : LoginPasswordViewController = LoginPasswordViewController()
        loginPasswordViewController.delegate = self
        self.navigationController.pushViewController(loginPasswordViewController, animated: true)
    }
}

extension LoginPasswordCoordinator : LoginPasswordViewControllerDelegate {
    
 
      // Navigate to Welcome page
      func navigateBackToLoginCPFPage() {
          self.delegate?.navigateBackToLoginCPFPage(newOrderCoordinator: self)
      }
    
}
