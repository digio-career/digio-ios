//
//  WelcomeCoordinator.swift
//  stageProject
//
//  Created by Everson Pereira Trindade on 03/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

class WelcomeCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let welcomeViewController: WelcomeViewController = WelcomeViewController()
        welcomeViewController.delegate = self
        self.navigationController.viewControllers = [welcomeViewController]
    }
}


extension WelcomeCoordinator: WelcomeViewControllerDelegate {

    // Navigate to next page
    func navigateToNextPage() {
       let loginCoordinator = LoginCoordinator(navigationController: navigationController)
       loginCoordinator.delegate = self
       childCoordinators.append(loginCoordinator)
       loginCoordinator.start()
    }
}

extension WelcomeCoordinator: BackToWelcomeViewControllerDelegate {
    
    // Back from third page
    func navigateBackToWelcomePage(newOrderCoordinator: LoginCoordinator) {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
}
