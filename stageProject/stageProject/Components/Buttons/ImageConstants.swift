//
//  ImageConstants.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 18/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

struct ImageConstants {
    
    // MARK: - Navgation
    static let goBack = UIImage(named: "navigation-back")
    static let goNext = UIImage(named: "navigation-next")
    
    static let help = UIImage(named: "help")
    
    static let eye = UIImage(named: "eye")
    static let eyeNo = UIImage(named: "eye-no")
    
    
    
}

