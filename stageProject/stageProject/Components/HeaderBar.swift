//
//  HeaderBar.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 16/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

protocol HeaderBarDelegate: class {
    func goBack()
    func goForward()
}

class HeaderBar: UIView {
     private weak var delegate: HeaderBarDelegate?
    
    @IBOutlet weak var contentView: UIView! {
        didSet {
            self.contentView.backgroundColor = .none
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel! 
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var backImage: UIImageView! {
        didSet {
            self.backImage.image = ImageConstants.goBack
            self.backImage.tintColor = .white
        }
    }
    
    @IBOutlet weak var helpImage: UIImageView! {
        didSet {
            self.helpImage.image = ImageConstants.help
        }
    }
    @IBOutlet weak var helpButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initXib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initXib()
    }
    
    private func initXib() {
        Bundle.main.loadNibNamed(String(describing: HeaderBar.self), owner: self, options: nil)
        contentView.setConstraintsOn(self)
    }
    
    func initHeader(delegate: HeaderBarDelegate?, title: String) {
        self.delegate = delegate
        self.titleLabel.text = title
   
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.goBack()
    }
    
    @IBAction func teste(_ sender: Any) {
        delegate?.goForward()
    }
    
}
