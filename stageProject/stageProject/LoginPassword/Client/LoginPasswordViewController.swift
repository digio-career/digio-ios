//
//  LoginPasswordViewController.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 18/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit


public protocol LoginPasswordViewControllerDelegate: class {
    func navigateBackToLoginCPFPage()
}

class LoginPasswordViewController: UIViewController, UITextFieldDelegate {
    
    private lazy var viewModel: LoginPasswordViewToModelProtocol = LoginPasswordViewModel(self)
    private var bottomConstraintNav: NSLayoutConstraint?

    
    @IBOutlet weak var subviewTopConstraint: NSLayoutConstraint!
     public weak var delegate: LoginPasswordViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            self.imageView.isHidden = false
        }
    }
    
    func setupView() {
        view.backgroundColor = .bottom_color
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = "Digite seu CPF:"
        }
    }
    
    @IBOutlet weak var passwordLabel: UILabel! {
        didSet {
            self.passwordLabel.text = "Senha:"
        }
    }
    
    @IBOutlet weak var switchLabel: UILabel! {
        didSet {
            self.switchLabel.text = "Lembrar CPF"
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            self.passwordTextField.isSecureTextEntry = true
        }
    }
   
    @IBOutlet weak var nextButtonImage: UIImageView! {
        didSet {
            self.nextButtonImage.image = ImageConstants.goNext
        }
    }
    
    @IBAction func nextPageButton(_ sender: Any) {
        self.showSpinner()
        viewModel.getUsersList()
        
    }
    
    @IBOutlet weak var showPasswordImage: UIImageView! {
        didSet {
            self.showPasswordImage.image = ImageConstants.eyeNo
        }
    }
    
    
    @IBOutlet weak var headerView: HeaderBar! {
        didSet {
            self.headerView.initHeader(delegate: self, title: "")
            self.headerView.backgroundColor = .bottom_color
        }
    }
    
    
    @IBAction func showPasswordAction(_ sender: Any) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        if showPasswordImage.image == ImageConstants.eyeNo {
            showPasswordImage.image = ImageConstants.eye
        }
        else {
            showPasswordImage.image = ImageConstants.eyeNo
        }
        
    }
    

}



extension LoginPasswordViewController {
@objc func keyboardWillShow(notification: NSNotification) {
    let keyboardInfo: NSDictionary = notification.userInfo! as NSDictionary
    let keyboardAnimationDuration = keyboardInfo.value(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as? Double ?? 0.3

    subviewTopConstraint.constant =  230.0
    
    UIView.animate(withDuration: keyboardAnimationDuration) {
        self.imageView.isHidden = true
        self.view.layoutIfNeeded()
        self.view.updateConstraints()
    }
}

@objc func keyboardWillHide(notification: NSNotification) {
    let keyboardInfo: NSDictionary = notification.userInfo! as NSDictionary
    let keyboardAnimationDuration = keyboardInfo.value(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as? Double ?? 0.3

    subviewTopConstraint.constant = 328.0
   
    UIView.animate(withDuration: keyboardAnimationDuration) {
        self.imageView.isHidden = false
        self.view.layoutIfNeeded()
        self.view.updateConstraints()
    }
}
    
}

extension LoginPasswordViewController: HeaderBarDelegate {
    func goBack() {
        self.delegate?.navigateBackToLoginCPFPage()
    }
    
    func goForward() {
        
    }
    
    
}

extension LoginPasswordViewController: LoginPasswordModelToViewProtocol {
    func didGetUsersList() {
        DispatchQueue.main.async {
            self.removeSpinner()
        }
    }
    
    func didNotGetUsersList(message: String) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
           UIAlertController(title: "ocorreu algum erro, tente novamente mais tarde", message: nil, preferredStyle: .actionSheet)
        }
        
    }
}
