//
//  LoginPasswordViewModel.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 29/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation

class LoginPasswordViewModel: LoginPasswordViewToModelProtocol {
    
    
    private weak var delegate: LoginPasswordModelToViewProtocol?
    private var userInfo: UserInfo?
    
    init(_ delegate: LoginPasswordModelToViewProtocol?) {
        self.delegate = delegate
    }
    
    func getUsersList() {
        
        LoginPasswordKitchen.getUsersList(completion: { response in
            self.userInfo = response
            self.delegate?.didGetUsersList()
            }) { err in
                self.delegate?.didNotGetUsersList(message: err ?? "ocorreu algum erro, tente novamente mais tarde")
            }
        }
    
  
}
