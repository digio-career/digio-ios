//
//  LoginPasswordProtocol.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 29/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation

protocol LoginPasswordViewToModelProtocol: class {
    func getUsersList()
}

protocol LoginPasswordModelToViewProtocol: class {
    func didGetUsersList()
    func didNotGetUsersList(message: String)
    
}
