//
//  ErrorResponse.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 03/08/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation

struct CodeError: Mappable {

    var statusCode: Int?
    var statusError: String?
    var path: String?
    var error: ErrorResponse?
}

struct ErrorResponse: Mappable{
    var code:String?
    var message:String
}
