//
//  GenericKitchen.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 03/08/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation

class GenericKitchen {
    static func getError(data: Data?) -> String {
        if let data = data {
            let object = data <--> CodeError.self
            return object?.error?.message ?? ""
        }
        return ""
    }
}
