//
//  LoginPasswordKitchen.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 29/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation
class LoginPasswordKitchen: GenericKitchen {
    
    static let rest = RestManager.sharedInstance
    static func getUsersList(completion: @escaping(_ responseObject: UserInfo?) -> Void, error: @escaping(_ error: String?) -> Void) {

        
        var header = RestManager.RestEntity()
        
        header.add(value: "$2b$10$Dp6BiiHuPgd5GVJ/.8ah2OsFh0cPeqjacuBe9K2YndGC6qN6FTCTy", forKey: "secret-key")
       
        rest.getUserInfo(headerParams: header) { result in
            if result.response?.httpStatusCode == 200 {
                completion(result.data <--> UserInfo.self)
            } else if result.response?.httpStatusCode == 400 {
                error(getError(data: result.data ?? Data()))
                
            }

        }
    }
}
