//
//  RestManager.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 27/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit
import Foundation
import CommonCrypto

@available(iOS 11.0, *)
class RestManager: NSObject {
    var requestHttpHeaders = RestEntity()
    
    var urlQueryParameters = RestEntity()
    
    var httpBodyParameters = RestEntity()
    var httpHeaderParameters = RestEntity()
    
    public static let sharedInstance = RestManager()
    
    var httpBody: Data?
    let url = URL(string: "https://api.jsonbin.io/b/5f20d00d91806166284b0489/1")
    
    
    
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        if urlQueryParameters.totalItems() > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            
            var queryItems = [URLQueryItem]()
            
            for (key, value) in urlQueryParameters.allValues() {
                let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                queryItems.append(item)
            }
            
            urlComponents.queryItems = queryItems
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        
        return url
    }
    
    
    private func getHttpBody(body: RestEntity?) -> Data? {
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") else { return nil }
        
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted, .sortedKeys])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, headerParams: RestEntity?, httpMethod: HttpMethod) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        for (header, value) in requestHttpHeaders.allValues() {
            request.setValue(value, forHTTPHeaderField: header)
        }
        if let hasExtraParams = headerParams {
            for (header, value) in hasExtraParams.allValues() {
                request.setValue(value, forHTTPHeaderField: header)
            }
        }
        if httpMethod != .get {
            request.httpBody = httpBody
        }
        return request
    }
    
 
    private func makeRequest(bodyParams: RestEntity?, headerParams: RestEntity?, url: URL,
                             withHttpMethod httpMethod: HttpMethod, image: Data? = nil,
                     completion: @escaping (_ result: Results) -> Void) {
        
        DispatchQueue.main.async {
            let targetURL = self.addURLQueryParameters(toURL: url)
            let httpBody = self.getHttpBody(body: bodyParams)
            
            guard let request = self.prepareRequest(withURL: targetURL, httpBody: httpBody, headerParams: headerParams, httpMethod: httpMethod) else
            {
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
        

            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data, response: Response(fromURLResponse: response), error: error))
                
             
                if error != nil {

                    print("erro", error)
                } else if let httpResponse = response as? HTTPURLResponse {
                
                    print("response" ,response)
                }
            }
            task.resume()
          
        }
    }
    
    public func getData(fromURL url: URL, completion: @escaping (_ data: Data?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
    
   public  func getUserInfo(headerParams: RestEntity, completion: @escaping (_ result: Results) -> Void) {

        self.urlQueryParameters.removeAllItems()
        
        self.requestHttpHeaders.add(value: "application/json;charset=UTF-8", forKey: "Content-Type")
        
        self.makeRequest(bodyParams: nil, headerParams: headerParams, url: url! , withHttpMethod: .get) { (results) in
            completion(results)
           
        }
        
    }
}
extension RestManager {
    struct RestEntity {
        private var values: [String: String] = [:]
        
        mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> String? {
            return values[key]
        }
        
        func allValues() -> [String: String] {
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
        
        mutating func removeAllItems() {
            values.removeAll()
        }
        
    }
    
    
    struct Response {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = RestEntity()
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    struct Results {
        var data: Data?
        var response: Response?
        var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }
    
    public enum CustomError: Error {
        case failedToCreateRequest
    }
    
    public struct capiError: Codable {
        public var message: String?
        public var fieldErrors: String?
        public var description: String?
    }
}


enum HttpMethod: String {
    case get
    case post
    case put
    case patch
    case delete
}


