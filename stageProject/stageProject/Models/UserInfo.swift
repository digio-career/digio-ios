//
//  UserInfo.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 27/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation

struct UserInfo: Mappable {
    var cpf: Int?
    var first_name: String?
    var last_name: String?
}
