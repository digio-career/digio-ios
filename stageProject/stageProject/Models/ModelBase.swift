//
//  ModelBase.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 27/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//


import Foundation

typealias Mappable = Codable & Equatable & Decodable & Encodable

infix operator <-->

func <--> <T: Mappable>(jsonData: Data?, type: T.Type) -> T? {
    guard let data = jsonData else {
        return nil
    }
    do {
        let decoder = JSONDecoder()
        let prod = try decoder.decode(T.self, from: data)
        return prod
    } catch let error {
        print("------ Error Decoder ------")
        print(error)
    }
    return nil
}

func jsonToData(json: Any?) -> Data? {
    if let dict = json as? NSDictionary {
        if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
            return data
        }
    }
    return Data()
}

func arrayToData(array: Any?) -> Data? {
    if let dict = array as? NSArray {
        if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
            return data
        }
    }
    return Data()
}
