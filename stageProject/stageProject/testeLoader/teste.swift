//
//  testeView.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 21/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import Foundation
import UIKit
class teste: UIViewController {
    var window = UIWindow()
    
    
    var test =  BallSpinFadeIndicator(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        addBallspin()
  
    }
    
    
    func addBallspin(){
        test.frame = CGRect(x: 0,
                                y: 0,
                                width: UIScreen.main.bounds.size.width/2,
                                height: UIScreen.main.bounds.size.height/2)
        self.view.addSubview(test)
        test.startAnimating()
    }
   
    
    
      func showActivityIndicatory() {
          let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
          actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
          actInd.center = self.view.center
          actInd.hidesWhenStopped = true
          actInd.style =
              UIActivityIndicatorView.Style.whiteLarge
          self.view.addSubview(actInd)
          actInd.startAnimating()
      }
}
