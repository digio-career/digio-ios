//
//  WelcomeViewController.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 02/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

public protocol WelcomeViewControllerDelegate: class {
    func navigateToNextPage()
}

class WelcomeViewController: UIViewController {
    
    public weak var delegate: WelcomeViewControllerDelegate?
    
    @IBOutlet weak var buttonNextPage: UIButton!{
        didSet {
                    self.buttonNextPage.setTitle("Começar", for: .normal)
                    self.buttonNextPage.setTitleColor(.white, for: .normal)
        //            self.buttonNextPage.titleLabel?.font = FontConstants.MonteserratMedium16
                    self.buttonNextPage.backgroundColor = .greenCapi
                    self.buttonNextPage.layer.cornerRadius = 8.0
        }
    }
    
    
    @IBAction func goToLogin(_ sender: Any) {
        self.delegate?.navigateToNextPage()

    }
    var statusBarLightStyle = true {
           didSet(newValue) {
               setNeedsStatusBarAppearanceUpdate()
           }
       }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
       
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.statusBarLightStyle = false
       }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    

}
