//
//  FontConstants.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 18/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//
import UIKit

struct FontConstants {
    static let QuicksandBookRegular16 = fontWith(name: "QuicksandBook-Regular", size: 16.0)
    

    static func fontWith(name: String, size: CGFloat) -> UIFont {
//      
//        if(UIDevice.current.screenType == .iPhones_5_5s_5c_SE) {
//            return UIFont(name: name, size: size*0.8 < 12 ? 12.0 : size*0.8)!
//        } else if (UIDevice.current.screenType == .iPhone4_4S) {
//            return UIFont(name: name, size: size*0.7 < 12 ? 12.0 : size*0.7)!
//        }
        
        return UIFont(name: name, size: size) ?? UIFont()
    }
    
}

