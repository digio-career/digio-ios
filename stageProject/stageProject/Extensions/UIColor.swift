//
//  UIColor.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 01/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

enum OpacityPreset {
    case a30
    case a50
    case a70
}

extension UIColor {
    
    // MARK: - Digio 2.0 Colors
    
    class var greenCapi: UIColor {
        return UIColor(hex: "2695a7")
    }
    
    class var bottom_color : UIColor {
        return UIColor(hex: "f4f6fa")
    }
    
    class var greenCapiLight : UIColor {
        return UIColor (hex: "37718e")
    }
    
    class var darkRose: UIColor {
        return UIColor(hex: "c33c54")
    }
    
    
    // MARK: - Convert HEX to RGB
    convenience init(hex: String, alpha: CGFloat? = 1.0) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: alpha ?? 1.0
        )
    }
    
    // MARK: - RGB to HEX String
    var hexString:String? {
        if let components = self.cgColor.components {
            let r = components[0]
            let g = components[1]
            let b = components[2]
            return  String(format: "%02X%02X%02X", (Int)(r * 255), (Int)(g * 255), (Int)(b * 255))
        }
        return nil
    }
}
