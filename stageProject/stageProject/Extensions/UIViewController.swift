//
//  UIViewController.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 22/07/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

fileprivate var aView : UIView?

extension UIViewController {
    
    func showSpinner() {
        aView = UIView(frame: self.view.bounds)
        aView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        
        let actInd = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        actInd.center = aView?.center ?? CGPoint()
        actInd.startAnimating()
        aView?.addSubview(actInd)
        self.view.addSubview(aView ?? UIView())
    }
    
    func removeSpinner() {
        aView?.removeFromSuperview()
        aView = nil
    }
    
    
}
