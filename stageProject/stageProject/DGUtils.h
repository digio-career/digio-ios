//
//  DGUtils.h
//  stageProject
//
//  Created by Thaina dos Santos Reis on 18/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//
import CommonCrypto
import DGUtils
import DGUtils.Swift
import Foundation
import SwiftOnoneSupport
import UIKit

public var DGUtilsVersionNumber: Double
open class BaseView : UIView {

    override public init(frame: CGRect)

    required public init?(coder aDecoder: NSCoder)
}

open class CurrencyUtils : NSObject {

    public static func formatCurrencyBRL(amount: Float) -> String

    public static func formatBRLtoFloat(text: String) -> Float

    public static func cleanValue(value: String?) -> String
}

open class DateUtils {

    public struct DateFormats {

        public static let yyyyMMdd: String

        public static let ddMMyyyy: String

        public static let ddMMyy: String

        public static let ddMM: String

        public static let ddMMM: String

        public static let yyyy: String
    }

    public static func isDateBefore(dateString: String, days: Int) -> Bool

    public static func calculateDateTimelineTransaction(dateString: String) -> String

    public static func calculateFullMonthYYDate(_ dateString: String, showYear: Bool) -> String

    public static func calculateInvoiceDetailDate(_ dateString: String) -> String

    public static func formatDateAs(inputDateFormat: String? = nil, dateString: String, outputDateFormat: String) -> String

    public static func formatDateAsShortenedMonth(_ dateString: String) -> String

    public static func getInvoiceYear(_ dateString: String) -> String

    public static func getNonSummerlightSavingsDt(dateString: String) -> Date

    public static func getNonSummerlightSavingsDate(format: String, dateString: String) -> String

    public static func canConvertStringToDate(data: String?, withFormat format: String = "dd/MM/yyyy") -> Bool

    public static func DateForPay(inputDate: String) -> String
}

public class Version {

    public static func currentVersion() -> String?
}

extension UIView {

    public func roundCorner(corners: UIRectCorner, radius: CGFloat)
}

extension UIDevice {

    @objc public static let modelName: String

    public var udid: String { get }

    public var screenHeightInPoints: CGFloat { get }

    public var screenWidthInPoints: CGFloat { get }

    public var iPhoneX_XS: Bool { get }

    public var iPhoneXR: Bool { get }

    public var iPhoneXSMax: Bool { get }

    public var isLargeScreen: Bool { get }

    public var iPhone: Bool { get }

    public var isIPhoneDisplayZoomed: Bool { get }

    public enum ScreenType : String {

        case iPhone4_4S

        case iPhones_5_5s_5c_SE

        case iPhones_6_6s_7_8

        case iPhones_6Plus_6sPlus_7Plus_8Plus

        case iPhoneX_XS

        case iPhoneXR

        case iPhoneXSMax

        case unknown
    }

    public var screenType: UIDevice.ScreenType { get }

    public var hasNotch: Bool { get }

    public func SYSTEM_VERSION_EQUAL_TO(version: String) -> Bool

    public func SYSTEM_VERSION_GREATER_THAN(version: String) -> Bool

    public func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: String) -> Bool

    public func SYSTEM_VERSION_LESS_THAN(version: String) -> Bool

    public func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version: String) -> Bool
}

extension String {

    public var digits: String { get }

    public func removeFormatAmount() -> Double

    public func getMonthFromStringDate(isMonthName: Bool = false) -> String

    public func formatAmericanDate() -> String

    public func currencyInputFormatting() -> String

    public func currencyTextToFloat() -> Float

    public func toDate() -> Date

    public func dateForWithdraw() -> Date

    public func dateForCalendar() -> Date?

    public func dateForOrders() -> String

    public func timeForOrders() -> String

    public func dateForVirtualCard() -> String

    public func dateForVirtualCardToBuy() -> String

    public func dateForWithdraw() -> String

    public func dateForInstallment() -> String

    public func dateForProfile() -> String

    public func nsRange(from range: Range<String.Index>) -> NSRange

    public func formatCardDueDate() -> String

    public func isOnlyNumbers() -> Bool

    public func isAlphanumeric() -> Bool

    public func formatCellphone() -> String

    public func firstName() -> String

    public func firstMonthLetters() -> String

    public func firstLetters() -> String

    public func getFormattedDate() -> Date
}

extension UILabel {

    public func countLabelLines() -> Int

    public func formatCurrencySuperscript(fontMedium: UIFont, fontSuper: UIFont, font: UIFont)

    public func formatCurrencyInvoice(fontMedium: UIFont, fontSuper: UIFont, font: UIFont)
}

