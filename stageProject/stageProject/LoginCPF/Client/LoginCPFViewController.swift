//
//  LoginCPFViewController.swift
//  stageProject
//
//  Created by Thaina dos Santos Reis on 18/06/20.
//  Copyright © 2020 Thaina dos Santos Reis. All rights reserved.
//

import UIKit

public protocol LoginCPFViewControllerDelegate: class {
    func navigateBackToWelcomePage()
    func navigateToNextPage()
    
}

class LoginCPFViewController: UIViewController {
    
    
    private var bottomConstraintNav: NSLayoutConstraint?
    private var CPF = ""
    
    
    @IBOutlet weak var subviewTopConstraint: NSLayoutConstraint!
    
    public weak var delegate: LoginCPFViewControllerDelegate?
    
    

    @IBOutlet weak var subView: UIView! {
        didSet {
            self.subView.backgroundColor = .white
            self.subView.layer.cornerRadius = 8.0
        }
    }
    
    @IBOutlet weak var subView2: UIView! {
        didSet {
            self.subView2.backgroundColor = .bottom_color
        }
        
    }
    
    @IBOutlet weak var headerView: HeaderBar! {
        didSet {
            self.headerView.initHeader(delegate: self, title: "")
            self.headerView.backgroundColor = .bottom_color
        }
    }
    
    @IBOutlet weak var nextButtonImg: UIImageView! {
        didSet {
            self.nextButtonImg.image = ImageConstants.goNext
            self.nextButtonImg.isHidden = true
        }
    }

    
    @IBOutlet weak var nextPageButton: UIButton! {
        didSet {
            self.nextPageButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            self.imageView.isHidden = false
        }
    }
    
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = "Digite seu CPF"
            self.titleLabel.textColor = .greenCapiLight
        }
    }
    
    @IBOutlet weak var dataTextField: UITextField! {
        didSet {
            
            self.dataTextField.delegate = self
            self.dataTextField.keyboardType = .numberPad
            self.dataTextField.textColor = .greenCapi
            self.dataTextField.tintColor = .black
            self.dataTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var infoLabel: UILabel!{
        didSet{
            self.infoLabel.text = "CPF inválido"
            self.infoLabel.textColor = .darkRose
        }
    }
    
    
    
    @IBOutlet weak var switchLabel: UILabel!{
        didSet{
            self.switchLabel.text = "Lembrar CPF"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        // Do any additional setup after loading the view.
    }
    func setupView() {
        self.infoLabel.isHidden = true
        view.backgroundColor = .bottom_color
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func validateCPF() -> Bool {
        if let cpf = dataTextField.text {
            if cpf.isValidCPF {
                return true
            }
        }
        infoLabel.isHidden = false
        return false
    }
    

  


    
    @IBAction func goToLoginPassword(_ sender: Any) {
        if validateCPF() {
            self.delegate?.navigateToNextPage()
            
        }
    }
    
  
}

extension LoginCPFViewController {
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardAnimationDuration = keyboardInfo.value(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as? Double ?? 0.3
        
        subviewTopConstraint.constant =  230.0
        
        UIView.animate(withDuration: keyboardAnimationDuration) {
            self.imageView.isHidden = true
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let keyboardInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardAnimationDuration = keyboardInfo.value(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as? Double ?? 0.3
        
        subviewTopConstraint.constant = 364.0
        
        UIView.animate(withDuration: keyboardAnimationDuration) {
            self.imageView.isHidden = false
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
        }
    }
    
}


extension LoginCPFViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
        }
        if (range.location == 3 || range.location == 7) {
            textField.text = textField.text! + "."
        } else if range.location == 11 {
            textField.text = textField.text! + "-"
        } else {
            if (textField.text?.count)! >= 14  {
                return false
            }
        }
        return true
    }
    @objc func textFieldDidChange(textField: UITextField) {
        if let cpf = textField.text {
            if cpf.count == 14 {
                CPF = cpf
                nextPageButton.isEnabled = true
                nextButtonImg.isHidden = false
            } else {
                 nextPageButton.isEnabled = false
                
            }
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            if text.count >= 6 {
                self.goToLoginPassword(self)
            }
        }
        return false
    }
    
}

extension LoginCPFViewController: HeaderBarDelegate {
    func goBack(){
        self.delegate?.navigateBackToWelcomePage()
    }
    func goForward(){
        let view = BallSpinView()
        self.navigationController?.pushViewController(view, animated: false)
    }
}
